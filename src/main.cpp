#include "platform/System.h"
#include "App.h"

int main(int argc, char** argv) {
    App app;
    sys::Run(&app);
}